import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import {Dossier} from './dossier';

@Component({
  selector: 'et-form',
  template: `
    <label for="name">Naam</label><input name="name" type="text" [(ngModel)]="dossier.subject"><br>
    <label for="description">Omschrijving</label><input name="description" type="text" [(ngModel)]="dossier.description"><br>
    <button (click)="save()">Opslaan</button>
  `
})
export class EtFormComponent implements OnInit {
  public dossier: Dossier;

  @Input() dossierNr: string;
  @Output() onSaved = new EventEmitter<any>();

  ngOnInit() {
    this.reset();
  }

  public save() {
    this.onSaved.emit(this.dossier.clone());
    this.reset();
  }

  private reset() {
    this.dossier = new Dossier('ET', this.dossierNr, '', '');
  }
}
