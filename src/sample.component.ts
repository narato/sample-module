import { Component } from '@angular/core';
// import { ValidationService } from 'abs-layout-renderer';
import { SampleService } from './sample.service';

@Component({
  selector: 'sample-component',
  template: `
    <br><br>
    <h3>Voorbeeld</h3>
    <button (click)="enableBtn = !enableBtn">Toggle</button>
    <button *ngIf="enableBtn" (click)="printMessage()">Click me!</button>
    <ul *ngFor="let message of messages"><li>{{message | samplePipe}}</li></ul>
  `
})
export class SampleComponent {
  public enableBtn = true;
  public messages = ['initial'];

  // constructor(private sampleService: SampleService, private validationService: ValidationService) {}
  constructor(private sampleService: SampleService) {}

  public printMessage() {
    console.log('A message printed by the sample component');
    this.messages.push(this.sampleService.sample());
    // this.messages.push(this.validationService.validate());
  }

}
