import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EtFormComponent } from './et-form.component';
import { SgwFormComponent } from './sgw-form.component';
import { SampleComponent } from './sample.component';
import { SampleDirective } from './sample.directive';
import { SamplePipe } from './sample.pipe';
import { SampleService } from './sample.service';

export * from './sample.component';
export * from './sample.directive';
export * from './sample.pipe';
export * from './sample.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    EtFormComponent,
    SgwFormComponent,
    SampleComponent,
    SampleDirective,
    SamplePipe
  ],
  exports: [
    EtFormComponent,
    SgwFormComponent,
    SampleComponent,
    SampleDirective,
    SamplePipe
  ]
})
export class SampleModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SampleModule,
      providers: [SampleService]
    };
  }
}
