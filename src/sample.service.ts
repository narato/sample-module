import { Injectable } from '@angular/core';

@Injectable()
export class SampleService {

  constructor() {}

  public sample(): string {
    return 'This is a message from the Sample Service';
  }
}
