export class Dossier {
    public type: string;
    public dossierNr: string;
    public subject: string;
    public description: string;

    constructor(
        type: string,
        dossierNr: string,
        subject: string,
        description: string
    ) {
        this.type = type;
        this.dossierNr = dossierNr;
        this.subject = subject;
        this.description = description;
    }

    clone() {
        return new Dossier(this.type, this.dossierNr, this.subject, this.description);
    }
}
